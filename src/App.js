//import { cleanup } from '@testing-library/react';
import React, {Fragment, useState, useEffect} from 'react';
import Navbar from './Components/Navbar';
import ProdList from './Components/ProdList';
import Form from './Components/Form';


function App() {

  const [producto, setProducto] = useState({
    descripcion:'',
    precio: '',
    stock:0
  })

  const [productos, setProductos] = useState([])

  const [listUpdated, setListUpdated] = useState(false)

  useEffect(() => {
    const getProductos = () => {
      fetch('http://localhost:9000/api')
      .then(res => res.json())
      .then(res => setProductos(res)) 
      //.then(res => console.log(res))

    }
    getProductos()
    setListUpdated(false)
  }, [listUpdated] )
   
return (
    <Fragment>
      <Navbar brand='Tienda App'/>
        <div className="container">
          <div className="row">
            <div className="col-7">
                <h2 style={{textAlign: 'center'}}>Lista de Productos</h2>
                <ProdList producto={producto} setProducto= {setProducto} productos={productos} setListUpdated={setListUpdated} />
            </div>
            <div className="col-5">
              <h2 style={{textAlign: 'center'}}>Agregar Producto</h2>
              <Form producto={producto} setProducto={setProducto}/>
            </div>
        

          </div>

        </div>
    </Fragment>

  );
}

export default App;
