import React from 'react';
import { Button, Input } from 'antd';
import 'antd/dist/antd.css';

const Form = ({producto, setProducto}) => {

    const handleChange = e => {
        setProducto({
            ...producto,
            [e.target.name]: e.target.value
        })
    }
    

    //destruccion de usestate producto
    let {descripcion, precio, stock} = producto

    const handleSubmit = () => {

        // convertir a entero el campo
        stock=parseInt(stock, 10)

        //validacion de los campos
        if (descripcion === '' || precio === '' || stock <= 0 ) {
            alert('Todos los campos son obligatorios')
            return
        }

        //consulta
        const requestInit = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(producto)
        }
        fetch('http://localhost:9000/api', requestInit)
        .then(res => res.text())
        .then(res => console.log(res))

         //reiniciando state de producto
         setProducto({
            descripcion: '',
            precio: '',
            stock: 0
        })

    } 



    return ( 
        <form onSubmit= {handleSubmit}>
            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">Descripcion</label>
                <Input value={descripcion} name="descripcion" onChange={handleChange} type="text" id="descripcion" placeholder="Descripcion" />
            </div>
            <div className="mb-3">
                <label htmlFor="precio" className="form-label">Precio</label>
                <Input value={precio} name="precio" onChange={handleChange} type="number" id="precio" placeholder="Precio"/>
            </div>
            <div className="mb-3">
                <label htmlFor="stock" className="form-label">Stock</label>
                <Input value={stock} name="stock" onChange={handleChange} type="number" id="stock" placeholder="Stock"/>
            </div>
        
            <Button type="primary" htmlType="submit">Agregar</Button>
        </form>

     );
}
 
export default Form;
