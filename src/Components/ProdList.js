import React from 'react';
import { Button } from 'antd';
import 'antd/dist/antd.css';





const ProdList = ({setProducto, producto, productos, setListUpdated}) => {

    /// eliminar registro
    const handleDelete = id => {
        console.log("eliminar");
        const requestInit = {
            method: 'DELETE'
        }
        fetch('http://localhost:9000/api/' + id, requestInit)
        .then(res => res.text())
        .then(res => console.log(res))

        setListUpdated(true)
    }

    // Actualizar registros
   let {descripcion, precio, stock} = producto

    const handleUpdate = id => {
        stock = parseInt(stock, 10)
        //validación de los datos
        if (descripcion === '' || precio === '' || stock <= 0 ) {
            alert('Todos los campos son obligatorios')
            return
        }
        const requestInit = {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(producto)
        }
        fetch('http://localhost:9000/api/' + id, requestInit)
        .then(res => res.text())
        .then(res => console.log(res))

        //reiniciando state de producto
        setProducto({
            descripcion: '',
            precio: '',
            stock: 0
        })

        setListUpdated(true)
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Stock</th>
                    <th>Fecha de Reg/Act</th>

                </tr>

            </thead>
            <tbody>
                {productos.map(producto =>(
                    <tr key={producto.id}>
                        <td>{producto.id}</td>
                        <td>{producto.descripcion}</td>
                        <td>{producto.precio}</td>
                        <td>{producto.stock}</td>
                        <td>{producto.fecha}</td>
                        <td>
                       
                        <div className="mb-3">
                            <Button onClick={() => handleDelete(producto.id)} type="dashed" >Eliminar</Button>
                            </div>
                            <div className="mb-3">
                            <Button onClick={() => handleUpdate(producto.id)}  type="primary">Modificar</Button>
                            </div>
                           
                        </td>
                    </tr>
                    ))}

            </tbody>
        </table>
    );
}

export default ProdList;
